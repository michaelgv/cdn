var express = require('express');
var app = express();
var port = process.env.PORT || 3000;

app.use('/static', express.static('static'));

app.listen(port, function () {
	console.log('Listening on port ' + port);
});
